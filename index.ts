import express from 'express';
import {router} from "./api";

const app = express();

app.use('/api', router);

// todo: get from config
const port = 8081;

app.listen(port, () => {
	console.log(`listening on port ${port}`)
});
