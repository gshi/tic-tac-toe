import {Router, ErrorRequestHandler} from 'express';
import gameRouter from './game/game.router';
const bodyParser = require('body-parser');

export const router = Router();

router.use(bodyParser.json())

router.use('/game', gameRouter);


const errorHandler: ErrorRequestHandler = (err, req, res, next) => {
	console.error('error!', err);
	res.status(500).send(err)
};
router.use(errorHandler);
