import {Handler} from "express";
import {TicTacToeService} from "../../tic-tac-toe/service";
import {InMemory} from "../../tic-tac-toe/store";
const store = new InMemory();
const service = new TicTacToeService(store);


export const createGame: Handler = (req, res, next) => {
	service.createGame()
		 .then(v => res.send(v))
		 .catch(err => next(err))
};

export const move: Handler = (req, res, next) => {
	const gameId = req.params.id;
	const {player, position} = req.body;

	service.makeMove(gameId, player, position)
		 .then(v => res.send(v))
		 .catch(err => next(err))
};

export const getStatus: Handler = (req, res, next) => {
	const gameId = req.params.id;

	service.getGameStatus(gameId)
		 .then(v => res.send(v))
		 .catch(err => next(err))
};
