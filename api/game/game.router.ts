import {Router} from "express";
import {createGame, getStatus, move} from "./game.controller";

const router = Router();

router.post('/', createGame);
router.put('/:id/move', move);
router.get('/:id/status', getStatus);

export default router;
