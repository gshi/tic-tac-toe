import {TicTacToeService} from "./service";
import {Player} from "./enums";
import {InMemory} from "./store";

const store = new InMemory();
let ticTacTowService: TicTacToeService;

beforeEach(() => {
	//todo: mock the store..
	ticTacTowService = new TicTacToeService(store)
});

describe('tic tac toe', () => {

	it('should set X as winner and display the correct status', async () => {
		const boardId = await ticTacTowService.createGame();

		await ticTacTowService.makeMove(boardId, Player.O, 0);
		await ticTacTowService.makeMove(boardId, Player.X, 1);
		await ticTacTowService.makeMove(boardId, Player.O, 3);
		await ticTacTowService.makeMove(boardId, Player.X, 2)
			 .then(v => expect(v).toEqual({isDone: false, winner: null}))


		await ticTacTowService.makeMove(boardId, Player.O, 6)
			 .then(v => expect({isDone: true, winner: Player.O}))

		await ticTacTowService.getGameStatus(boardId).then(v => {
			expect(v).toEqual({isDone: true, winner: Player.O})
		})

	});

	it('should declare tie', async () => {
		const boardId = await ticTacTowService.createGame();

		await ticTacTowService.makeMove(boardId, Player.X, 0);
		await ticTacTowService.makeMove(boardId, Player.O, 2);
		await ticTacTowService.makeMove(boardId, Player.X, 1);
		await ticTacTowService.makeMove(boardId, Player.O, 3);
		await ticTacTowService.makeMove(boardId, Player.X, 5);
		await ticTacTowService.makeMove(boardId, Player.O, 4);
		await ticTacTowService.makeMove(boardId, Player.X, 6);
		await ticTacTowService.makeMove(boardId, Player.O, 7);
		await ticTacTowService.makeMove(boardId, Player.X, 8);


		await ticTacTowService.getGameStatus(boardId).then(v => {
			expect(v).toEqual({isDone: true, winner: null})
		})

	});


	it('should return error if position is invalid', async () => {
		const boardId = await ticTacTowService.createGame();

		await expect(ticTacTowService.makeMove(boardId, Player.O, -1)).rejects.toBeTruthy()
		await expect(ticTacTowService.makeMove(boardId, Player.O, 9)).rejects.toBeTruthy()
	});

})
