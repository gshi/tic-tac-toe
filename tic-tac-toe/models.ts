import {Player} from "./enums";


export interface IGameStatus {
	isDone: boolean;
	winner: Player;
}
