import {Player} from "./enums";


export interface IStore {
	createBoard(): Promise<string>

	getBoard(id: string): Promise<Player[]>;

	setTile(boardId: string, player: Player, position: number): Promise<void>;
}


//copy pasted...
function makeid(length: number) {
	let result = '';
	const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	const charactersLength = characters.length;
	for (let i = 0; i < length; i++) {
		result += characters.charAt(Math.floor(Math.random() *
			 charactersLength));
	}
	return result;
}

export class InMemory implements IStore {
	boards: { [id: string]: (Player | null)[]  } = {};

	createBoard(): Promise<string> {
		//todo: use uuid
		const id = makeid(4);
		this.boards[id] = [null, null, null, null, null, null, null, null, null];
		return Promise.resolve(id);
	}

	getBoard(id: string): Promise<Player[]> {
		const board = this.boards[id];

		if (!board) {
			return Promise.reject("board not found")
		}
		return Promise.resolve(board.slice() as Player[]);
	};

	setTile(boardId: string, player: Player, position: number): Promise<void> {
		if (!this.boards[boardId]) {
			return Promise.reject('board not found')
		}

		this.boards[boardId][position] = player;
		return Promise.resolve()
	}

}
