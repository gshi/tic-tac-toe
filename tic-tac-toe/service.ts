import {Player} from "./enums";
import {IGameStatus} from "./models";
import {IStore} from "./store";


export class TicTacToeService {
	constructor(private store: IStore) {
	}

	createGame(): Promise<string> {
		return this.store.createBoard()
	}

	makeMove(boardId: string, player: Player, position: number): Promise<IGameStatus> {
		if (position > 8 || position < 0) {
			return Promise.reject('invalid position')
		}

		//todo: validate correct turn


		return this.store.setTile(boardId, player, position)
			 .then(v => this.getGameStatus(boardId))
	}

	getGameStatus(boardId: string): Promise<IGameStatus> {
		const winStates: number[][] = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]];

		return this.store.getBoard(boardId)
			 .then(board => {
				 for (const state of winStates) {
					 if (board[state[0]] && board[state[0]] === board[state[1]] && board[state[1]] === board[state[2]]) {
						 return {isDone: true, winner: board[state[0]]}
					 }
				 }

				 return {isDone: board.every(v => v), winner: null as unknown as Player}
			 })
	}
}
